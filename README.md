# ska-cookiecutter-pypackage

Quickstart
----------

SKA python projects use `poetry` for packaging etc. Make sure that you have it
installed.

Install the latest Cookiecutter if you haven't installed it yet (this requires
Cookiecutter 1.4.0 or higher):

    pip install -U cookiecutter

To generate a SKA Python package project of your own, go to the directory where you want to
store it and then run:

    # e.g. cd /tmp/
    cookiecutter https://gitlab.com/ska-telescope/templates/ska-cookiecutter-pypackage/

It will ask a few questions, which need to be filled in - most of the defaults are examples
so take care answering them:


| Field                     | Description                                                                |
|---------------------------|----------------------------------------------------------------------------|
| full_name                 | Your full name                                                             |
| email                     | Your email                                                                 |
| institution               | Your institution/organisation                                              |
| gitlab_username           | Your user name from gitlab                                                 |
| project_name              | The hypehenated project name with *ska-* prefix                            |
| project_slug              | The pyhton package name/slug (defaults to *project_name* with underscores) |
| project_short_description | Project description for packge                                             |
| version                   | Package version to use                                                     |
| use_mypy                  | use mypy for extra code quality                                            |
| open_source_license       | one ofBSD-3-Clause,MIT,Apache-2.0,Proprietary                              |
| create_git_repo           | Whether to create/update a git repository                                  |


It is recommended to use the git repo creation when starting from scratch.
After creating the repository, ask in the `#team-system-support` slack
channel for an **empty** project to be created using the *project name* you have chosen during
the project creation above. Also provide them with your *gitlab* user name and that you
will be the **maintainer** of the project.

Once you have received the confirmation of the URL, you can push your repository to gitlab:

```bash
git remote add origin https://gitlab.com:ska-telescope/<my_project_name>.git
git push -u origin main
```

You now have a bootstrapped project including a *gitlab* pipeline which you can
populate with your code. Please follow the developer guidelines when doing so.

