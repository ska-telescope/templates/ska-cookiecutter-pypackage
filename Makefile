BAKE_OPTIONS=--no-input -v

bake:
	cookiecutter $(BAKE_OPTIONS) . --overwrite-if-exists

include .make/base.mk
include .make/python.mk

PYTHON_SRC = hooks

DOCS_SPHINXOPTS = -W --keep-going

docs-pre-build:
	poetry config virtualenvs.create false
	poetry install --no-root --only docs

PYTHON_LINE_LENGTH = 88

python-post-lint:
	mypy $(PYTHON_SRC) tests/

.PHONY: docs-pre-build python-post-lint