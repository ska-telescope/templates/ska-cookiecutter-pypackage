#!/usr/bin/env python

"""Tests for `{{ cookiecutter.project_slug }}` package."""

import pytest

import {{cookiecutter.project_slug}}


# This is a sample pytest test fixture
# See more at: http://doc.pytest.org/en/latest/fixture.html
@pytest.fixture(name="version")
def version_fixture() -> str:
    """Return the package's version string.

    :returns: package version string
    """
    return {{cookiecutter.project_slug}}.__version__


# This is a sample pytest test function with the pytest fixture as an argument.
def test_content(version: str) -> None:
    """
    Check that the package version is as expected.

    :param version: the version fixture
    """
    assert version == "{{cookiecutter.version}}"
