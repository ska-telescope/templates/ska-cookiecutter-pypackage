{% set is_open_source = cookiecutter.open_source_license != 'Proprietary' -%}
# {{ cookiecutter.project_name }}

{{ cookiecutter.project_short_description }}

{% if is_open_source %}
## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-{{ cookiecutter.project_name }}/badge/?version=latest)](https://developer.skao.int/projects/{{ cookiecutter.project_name }}/en/latest/?badge=latest)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, or browsed in the SKA development portal:

* [{{ cookiecutter.project_name }} documentation](https://developer.skatelescope.org/projects/{{  cookiecutter.project_name }}/en/latest/index.html "SKA Developer Portal: {{  cookiecutter.project_name }} documentation")


{%- endif %}

## Features

* TODO
