{%- set title = cookiecutter.project_slug+" Package" -%}
{%-  set n = title|length -%}
{{ title }}
{{ n*"=" }}

.. automodule:: {{ cookiecutter.project_slug }}
    :members:
    :special-members:
