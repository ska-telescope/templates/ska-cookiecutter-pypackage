{%- set title = "Welcome to "+cookiecutter.project_name+"'s documentation!" -%}
{%-  set n = title|length -%}
{{ title }}
{{ n*"=" }}

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   api/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
