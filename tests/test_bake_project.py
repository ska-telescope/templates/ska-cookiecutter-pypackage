"""Test module using bake."""
import datetime
import os
import shlex
import subprocess
import typing
from contextlib import contextmanager

import pytest
from cookiecutter.utils import rmtree


@contextmanager
def inside_dir(dirpath: str) -> typing.Generator[None, None, None]:
    """Context manager to execute code from inside the given directory.

    :param dirpath: String, path of the directory the command is being run.
    :yield: context
    """
    old_path = os.getcwd()
    try:
        os.chdir(dirpath)
        yield
    finally:
        os.chdir(old_path)


@contextmanager
def bake_in_temp_dir(
    cookies: typing.Any, *args: typing.Any, **kwargs: typing.Any
) -> typing.Generator[typing.Any, None, None]:
    """Context manager to delete the temporal directory.

    It is created when executing the tests and needs to be tidied up.

    :param cookies: cookie fixture to be baked and its temporal files will be removed
    :param args: pass through
    :param kwargs: pass through
    :yield: bake result
    """
    result = cookies.bake(*args, **kwargs)
    try:
        yield result
    finally:
        rmtree(str(result.project_path))


def run_inside_dir(command: str, dirpath: str) -> int:
    """Run a command from inside a given directory.

    This returns the exit status.

    :param command: Command that will be executed
    :param dirpath: path of the directory the command is being run
    :return: exit code
    """
    with inside_dir(dirpath):
        return subprocess.check_call(shlex.split(command))


def check_output_inside_dir(command: str, dirpath: str) -> bytes:
    """Run a command from inside a given directory.

    This return std output.
    :param command: the unix command
    :param dirpath: the directory to run in
    :return: process output
    """
    with inside_dir(dirpath):
        return subprocess.check_output(shlex.split(command))


def test_year_compute_in_license_file(cookies: typing.Any) -> None:
    """Test generation of year in templates.

    :param cookies: cookies fixture
    """
    with bake_in_temp_dir(cookies) as result:
        license_file_path = result.project_path.joinpath("LICENSE")
        now = datetime.datetime.now()
        assert str(now.year) in license_file_path.read_text()


def test_bake_not_open_source(cookies: typing.Any) -> None:
    """Test handle of license for proprietary projects.

    :param cookies: cookies fixture
    """
    with bake_in_temp_dir(
        cookies, extra_context={"open_source_license": "Proprietary"}
    ) as result:
        found_toplevel_files = [f.name for f in result.project_path.iterdir()]
        assert "pyproject.toml" in found_toplevel_files
        assert "LICENSE" not in found_toplevel_files


def test_bake_with_defaults(cookies: typing.Any) -> None:
    """Test baking and running the tests using defaults.

    :param cookies: cookies fixture
    """
    with bake_in_temp_dir(cookies) as result:
        assert result.project_path.is_dir()
        assert result.exit_code == 0
        assert result.exception is None

        found_toplevel_files = [f.name for f in result.project_path.iterdir()]
        assert "pyproject.toml" in found_toplevel_files
        # assert "ska_python_boilerplate" in found_toplevel_files
        assert "Makefile" in found_toplevel_files
        assert "tests" in found_toplevel_files


def test_bake_and_run_tests(cookies: typing.Any) -> None:
    """Test baking and running the tests.

    :param cookies: cookies fixture
    """
    with bake_in_temp_dir(cookies) as result:
        assert result.project_path.is_dir()
        assert run_inside_dir("poetry install", str(result.project_path)) == 0
        assert run_inside_dir("poetry run pytest", str(result.project_path)) == 0
        print("test_bake_and_run_tests path", str(result.project_path))


def test_bake_and_run_tests_with_nondefault_version(cookies: typing.Any) -> None:
    """Test baking and running the tests.

    :param cookies: cookies fixture
    """
    with bake_in_temp_dir(cookies, extra_context={"version": "1.2.3"}) as result:
        assert result.project_path.is_dir()
        assert run_inside_dir("poetry install", str(result.project_path)) == 0
        assert run_inside_dir("poetry run pytest", str(result.project_path)) == 0
        print("test_bake_and_run_tests path", str(result.project_path))


def test_bake_withspecialchars_and_run_tests(cookies: typing.Any) -> None:
    """Ensure that a `full_name` with double quotes does not break poetry.

    :param cookies: cookies fixture
    """
    with bake_in_temp_dir(
        cookies, extra_context={"full_name": 'name "quote" name'}
    ) as result:
        assert result.project_path.is_dir()
        assert run_inside_dir("poetry install", str(result.project_path)) == 0
        assert run_inside_dir("poetry run pytest", str(result.project_path)) == 0


def test_bake_with_apostrophe_and_run_tests(cookies: typing.Any) -> None:
    """Ensure that a `full_name` with apostrophes does not break poetry.

    :param cookies: cookies fixture
    """
    with bake_in_temp_dir(cookies, extra_context={"full_name": "O'connor"}) as result:
        assert result.project_path.is_dir()
        assert run_inside_dir("poetry install", str(result.project_path)) == 0
        assert run_inside_dir("poetry run pytest", str(result.project_path)) == 0


# def test_make_help(cookies):
#     with bake_in_temp_dir(cookies) as result:
#         # The supplied Makefile does not support win32
#         if sys.platform != "win32":
#             output = check_output_inside_dir("make help", str(result.project))
#             assert b"include .make/python.mk" in output


def test_bake_selecting_license(cookies: typing.Any) -> None:
    """Test license selection.

    :param cookies: cookies fixture
    """
    license_strings = {
        "BSD-3-Clause": "Redistributions of source code must retain the "
    }

    for license_string, target_string in license_strings.items():
        with bake_in_temp_dir(
            cookies, extra_context={"open_source_license": license_string}
        ) as result:
            assert target_string in result.project_path.joinpath("LICENSE").read_text()
            assert (
                license_string
                in result.project_path.joinpath("pyproject.toml").read_text()
            )


@pytest.mark.parametrize("use_mypy,expected", [("y", True), ("n", False)])
def test_mypy(cookies: typing.Any, use_mypy: str, expected: bool) -> None:
    """Test mypy addition toggle.

    :param cookies: cookies fixture
    :param use_mypy: mypy flag
    :param expected: expected outcome
    """
    with bake_in_temp_dir(cookies, extra_context={"use_mypy": use_mypy}) as result:
        assert result.project_path.is_dir()
        requirements = result.project_path.joinpath("pyproject.toml")
        assert ("mypy" in requirements.read_text()) is expected
        makefile_path = result.project_path.joinpath("Makefile")
        assert ("mypy" in makefile_path.read_text()) is expected


@pytest.mark.parametrize("create_git_repo,expected", [("y", True), ("n", False)])
def test_git_repo(cookies: typing.Any, create_git_repo: str, expected: bool) -> None:
    """Test git repo creation toggle.

    :param cookies: cookies fixture
    :param create_git_repo: repo flag
    :param expected: expected outcome
    """
    with bake_in_temp_dir(
        cookies, extra_context={"create_git_repo": create_git_repo}
    ) as result:
        assert result.project_path.is_dir()
        git_dir = result.project_path.joinpath(".git")
        assert git_dir.exists() is expected
        modules_file = result.project_path.joinpath(".gitmodules")
        assert modules_file.exists() is expected
