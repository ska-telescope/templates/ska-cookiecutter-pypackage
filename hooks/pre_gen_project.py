"""Cookiecutter pre generation hook."""
import re
import sys

MODULE_REGEX = r"^[_a-zA-Z][_a-zA-Z0-9]+$"
SKA_MODULE_PREFIX = "ska_"


def main() -> int:
    """Check main entry point.

    :return: exit code
    """
    module_name = "{{ cookiecutter.project_slug }}"

    if not re.match(MODULE_REGEX, module_name):
        print(
            f"ERROR: The project slug ({module_name}) is not a valid "
            "Python module name. Please do not use a - and use _ instead"
        )
        sys.exit(1)
    elif not module_name.startswith(SKA_MODULE_PREFIX):
        print(
            f"ERROR: The project slug ({module_name}) needs to be prefixed "
            f"with '{SKA_MODULE_PREFIX}'."
        )
        sys.exit(1)
    return 0


if __name__ == "__main__":
    main()
