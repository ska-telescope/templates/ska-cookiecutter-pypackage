#!/usr/bin/env python
"""Cookiecutter post generation hook."""
import os
from pathlib import Path

PROJECT_DIRECTORY = Path().absolute()


def exists_in_project(path_name: str) -> bool:
    """Check if a path exists in project.

    :param path_name: the path to check
    :return: True or False
    """
    proj_path = PROJECT_DIRECTORY / path_name
    return proj_path.exists()


def do_git() -> None:
    """Apply git commands."""
    branch = "main"
    if not exists_in_project(".git"):
        os.system(f"git init && git checkout -b {branch}")
    else:
        branch = "ska-cookiecutter"
        os.system(f"git checkout -b {branch}")

    if not exists_in_project(".make"):
        make_url = "https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile.git"
        os.system(f"git submodule add {make_url} .make")

    os.system("git add .")
    os.system("git commit -m 'bootstrap of {{ cookiecutter.project_name }}'")
    if branch != "main":
        os.system(f"git checkout main && git merge {branch}")


GIT_INSTRUCTIONS = """
cd {{ cookiecutter.project_name }}
git init
git checkout -b main
git submodule add https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile.git .make
git add .
git commit -m "bootstrap of {{ cookiecutter.project_name }}"
"""

if __name__ == "__main__":
    if "{{ cookiecutter.open_source_license }}".lower() == "proprietary":
        os.remove(PROJECT_DIRECTORY / "LICENSE")

    if "{{ cookiecutter.create_git_repo }}".lower() == "y":
        do_git()
    else:
        print("To fully enable the project on gitlab you should execute:")
        print(GIT_INSTRUCTIONS)
